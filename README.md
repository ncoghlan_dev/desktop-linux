Desktop Linux Notes
===================

I've used desktop Linux since 2004, after getting frustrated with the
complexities of compiling CPython on Windows
[back then](https://mail.python.org/pipermail/python-dev/2004-July/046325.html),
tinkering with compatibility layers like Cygwin and coLinux, and finally
settling on running completely separate machines for gaming (Windows), and
everything else (Linux).

For most of that time, my approach has been to find a Windows machine with
hardware that I like, then install Linux on it. Since I've been using
Windows machines since they were actually MS-DOS machines (and have hence never
become accustomed to macOS interaction models), and don't have a particularly
wonderful rote memory (and have hence never seen the appeal of relying entirely
on the keyboard for system interaction), I've also always preferred KDE as my
primary Linux desktop environment.

For my latest machine, I chose to instead buy a system from a Linux specialist
vendor.


System 76, Galaga Pro (Fedora 27, KDE Spin)
-------------------------------------------

I don't actually have this system yet, but it should arrive soon.

I was going to put Fedora Atomic Workstation on it, but realised that
combining Fedora Atomic with my preference for KDE would likely be a problem
for something I'm planning to use as my primary work machine.

Instead, I'll likely switch the HP Spectre x360 over to Atomic Workstation
and trunk builds of KDE, and use that to start learning more about KDE and
Linux internals.


HP Spectre x360 (Fedora 26, KDE Spin)
--------------------------------------

I still think the Spectre is a lovely piece of hardware, but the state of its
Linux driver support is less than ideal.

I see (apparently harmless) ACPI errors in the logs from the `hp_wmi` module,
and need to do the following to get sound working reliably:

1. `sudoedit /etc/default/grub` to add
   `acpi_backlight=vendor acpi_osi='!Windows 2013' acpi_osi='!Windows 2012'`
   to the end of the `GRUB_CMDLINE_LINUX` entry
2. `sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg` to configure those
   settings for the next boot (to guard against errors, you may want to make
   a backup of the existing grub config file first)

https://bugs.freedesktop.org/show_bug.cgi?id=102765#c13 suggests that the
sound drivers should be fine given Linux kernel 4.13 or newer, and the
ACPI errors that show up in `dmesg` can be silenced by running
`sudoedit /etc/modprobe.d/blacklist.conf` and adding the line
`blacklist hp_wmi`. However, in that configuration, my own system was *really*
unstable (even worse problems with the internal monitor than those linked below,
and internal audio still wasn't working - only HDMI audio through the monitor)

I've never had any problems with the external display support, but unfortunately
the same can't be said for the internal display:
https://bugs.freedesktop.org/show_bug.cgi?id=102765

I've also seen problems with the internal keyboard intermittently disabling
itself (potentially related to the `hp_wmi` ACPI errors mentioned above:
https://bugzilla.redhat.com/show_bug.cgi?id=1275280
